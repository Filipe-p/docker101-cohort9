# Docker 101 

This repo is an intro to containers and docker!
Read on!!🚀🐳

### What is a Container? 
Container is a self contained environment that run a system. 
It sits above the physical layer and Operation system. It allows multiple containers to run inside one VM and one Operating system. 

As it sits above the OS, in theory it is portable to any system/os in practice it just *more portable* - For example stuff with the M1 run funny.  


### Container VS VM
VM need to setup physical infrastructure + OS where as container sits on top of these and shares resources.

Hence not a fair comparison but still useful to make as we use them get the same results - Working system.

### Docker

Docker is of many containarization technologies. 
Also the most used. 

### Other containarization services 

There are other, but docker is the standard. 

- Containerd

Kubernetes and EKS and PODMAN are container orchestration and deployment tools. Not the containers itself.


## Docker 

Docker is the containarization tool.

It pull images from dockerhub. 

Docker hub hosts many images! Like ngix or httpd.

main commands
```bash

# To start a container user docker run
$  docker run -d -p 80:80 docker/getting-started

# -d is for dettached mode 
# -p is port mapping [outside_container]:[inside_container]

# Check containers running 
$ docker ps 

# Stop a container 
$ docker stop 

# Docker images to see images in computer
$ docker images


```


**task**

- go get a image of httpd 
- how can you "ssh" into your docker container 
- have a look around 

- Start another server with nginx on a different port 

- look at opiton `-dit`what does that do?

- How do you remove theses images form your computer? 